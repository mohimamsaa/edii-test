package com.example.exercise.service;

import java.util.List;

import com.example.exercise.model.PegawaiModel;

public interface PegawaiService {
  List<PegawaiModel> getAllPegawai();
  PegawaiModel getPegawai(long id);
  PegawaiModel savePegawai(PegawaiModel pegawai);
  void deletePegawai(PegawaiModel pegawai);
}
