package com.example.exercise.service;

import java.util.List;
import java.util.Optional;

import com.example.exercise.model.PegawaiModel;
import com.example.exercise.repository.PegawaiDb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PegawaiServiceImpl implements PegawaiService {

  @Autowired
  private PegawaiDb pegawaiDb;

  @Override
  public List<PegawaiModel> getAllPegawai() {
    // TODO Auto-generated method stub
    return pegawaiDb.findAll();
  }

  @Override
  public PegawaiModel getPegawai(long id) {
    // TODO Auto-generated method stub
    Optional<PegawaiModel> pegawai = pegawaiDb.findById(id);
    if (pegawai.isPresent()) {
      return pegawai.get();
    } else {
      return null;
    }
  }

  @Override
  public PegawaiModel savePegawai(PegawaiModel pegawai) {
    // TODO Auto-generated method stub
    return pegawaiDb.save(pegawai);
  }

  @Override
  public void deletePegawai(PegawaiModel pegawai) {
    // TODO Auto-generated method stub
    pegawaiDb.delete(pegawai);
  }
  
}
