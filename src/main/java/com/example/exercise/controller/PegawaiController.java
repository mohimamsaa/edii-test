package com.example.exercise.controller;

import java.util.List;

import com.example.exercise.model.PegawaiModel;
import com.example.exercise.service.PegawaiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PegawaiController {

  @Autowired
  private PegawaiService pegawaiService;

  @RequestMapping(value = "/home", method = RequestMethod.GET)
  private String main(Model model) {
    List<PegawaiModel> allPegawai = pegawaiService.getAllPegawai();
    model.addAttribute("allPegawai", allPegawai);
    return "addPegawai";
  }
 
  @RequestMapping(value = "/get-all", method = RequestMethod.GET)
  public @ResponseBody List<PegawaiModel> getAllPegawai () {
    return pegawaiService.getAllPegawai();
  }

  @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
  public @ResponseBody PegawaiModel getPegawai (@PathVariable String id) {
    PegawaiModel pegawai = pegawaiService.getPegawai(Long.parseLong(id));
    return pegawai;
  }

  @RequestMapping(value = "/post", method = RequestMethod.POST)
  public @ResponseBody PegawaiModel savePegawai (@RequestBody PegawaiModel pegawai) {
    return pegawaiService.savePegawai(pegawai);
  }

  @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
  public String deletePegawai (@PathVariable String id) {
    PegawaiModel pegawai = pegawaiService.getPegawai(Long.parseLong(id));
    if (pegawai == null) {
      return "Pegawai tidak ada";
    }
    pegawaiService.deletePegawai(pegawai);
    return "Pegawai dengan nama " + pegawai.getName() + " berhasil di hapus";
  }
}
