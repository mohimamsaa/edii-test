package com.example.exercise.repository;

import com.example.exercise.model.PegawaiModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * FlightDb
 */
@Repository
public interface PegawaiDb extends JpaRepository<PegawaiModel, Long> {
}